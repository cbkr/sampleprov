import 'dart:async';

import 'package:flutter/cupertino.dart';

class AppState extends ChangeNotifier {
  bool _isLoggedIn = false;

  get isLoggedIn => _isLoggedIn;

  AppState();

  Future<void> simulateLogin() async {
    Timer(const Duration(milliseconds: 400), () {
      _isLoggedIn = true;
      notifyListeners(); 
    });
  }

  Future<void> simulateLogout() async {
    _isLoggedIn = false;
    notifyListeners();
  }
}
